//Jakob Jasin, jaj222@lehigh.edu, Prog1



import java.util.Arrays;
import java.util.Scanner;
public class Square{
        public static void sumrowsandcols(int[][] a){
            int sumOfR = 0;
    int sumOfC = 0;

System.out.print(Arrays.toString(a));
System.out.println();

/*prints out the sum of each row by addingvalues of the row together, printing
 the value and then replacing the value to zero
when moving to a new row.*/
        for (int i = 0; i < 5; i++)
        {
            for (int j = 0; j < 5; j++)
            {
                sumOfR = sumOfR + a[i][j]; 
            }

            
            System.out.println(sumOfR);
            sumOfR = 0;
        }
        //visually separates lines
        System.out.println();
        System.out.println();
        //sums the columns and then refreshes container value after a new column is moved to.
        for (int j = 0; j < 5; j++)
        {
            for (int i = 0; i < 5; i++)
            {
                sumOfC = sumOfC + a[i][j]; 
            }
            System.out.println(sumOfC);
            sumOfC = 0;
        }
    }


    public static void main(String[] args){
//Creates scanner for input, and array from said inputs
Scanner scan = new Scanner(System.in);
 int[][] arr = new int[5][5];
for (int i = 0; i < 5; i++)
      {
          for (int j = 0; j < 5; j++)
          {
              arr[i][j] = scan.nextInt();
          }
      }
      //calls the method
sumrowsandcols(arr);

    }
}